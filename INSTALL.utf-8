                     Gauche-gl - Gauche OpenGL バインディング

Gauche-glの最新情報は以下のURLで得られます。
  http://practical-scheme.net/gauche/


必要な準備
----------------------------------------------------------


* Gauche 0.9.3 以降がインストールされている必要があります。
* OpenGL 1.1以降もしくはMesa 3.0以降が必要です。
* GLUT 3.7以降が必要です。(ただし、MinGW上でのコンパイル時には
  必要ありません。以下で説明します)


簡単な方法
----------------------------------------------------------


   % ./configure
   % make
   % make install
Configureスクリプトは、既にインストールされているGaucheの情報を元に、
必要なコンパイラオプションやインストール先を設定します。


GLUTライブラリの場所
----------------------------------------------------------


もしGLUTライブラリが標準でない場所にインストールされている場合、以下のオプションで
その場所をconfigureスクリプトに渡して下さい。

  ./configure --with-glut=DIR


MinGWでは、別にGLUTをインストールするかわりに、ソースツリーに含まれるfreeglut-2.6
を使うこともできます。この方法を使った場合、freeglutライブラリはスタティックに
リンクされるため、スクリプトを配布する際にglutライブラリを別に含める必要は
ありません。この方法を選ぶ場合は次のオプションでconfigureしてください。

  ./configure --with-glut=mingw-static


freeglutをリンクしたlibgauche-glut.dllをアプリケーションと共に配布する場合は、
win32/freeglut-2.6.0-3/Copying.txt をチェックしてください。freeglutは
MITライセンス的な条件で使えますが、配布物には著作権表示を含めなければなりません。


NVidia Cg バインディング
----------------------------------------------------------


Issac Trotts氏による、NVidiaのCg Toolkitを使うためのバインディングが
オプションで提供されています。まだ一部のAPIしかカバーされていませんが
いくつかのサンプルを走らせることはできます。Cgバインディングをビルド、
インストールするためには、configure時に--enable-cgを指定してください。

  ./configure --enable-cg

ビルドには、NVidiaのCg Toolkitが既にインストールされている必要があります。
NVidiaのCg Toolkitは次のURLからダウンロードできます。

  http://developer.nvidia.com/object/cg_toolkit.html


プラットフォーム特有の情報
----------------------------------------------------------


FreeBSDのportsではMesaがpthreadsをイネーブルにしてコンパイルされている
そうなので、Gauche本体もpthreadsをイネーブルにしておかないとGauche-glが
リンクできません。(pthreadsを指定するとGaucheのconfigure時に警告が
出ますが、プログラム中でmake-threadしなければ全く問題ありません)


OSX 10.9以降ではGLUTが非推奨となったため、コンパイル中にWarningが大量に出ます。
動作に問題はありません。近い将来代替手段を提供する予定です。
